## DevKit for GitLab

[![pipeline status](https://gitlab.com/dosuken123/thin-gdk/badges/master/pipeline.svg)](https://gitlab.com/dosuken123/thin-gdk/commits/master)

This project is inspired by [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit) and [GitLab Compose Kit](https://gitlab.com/ayufan/gitlab-compose-kit).
It's trying to remove the complexity of GDK and unstableness of GCK and adopt the solidness of GDK and simplicity of GCK.

## Direction

- **Understand GitLab deeply**:
  - You set up micro-services by yourself, and you run those services with the clear intention. There are no magic here, like `gdk run` does..
  - Learn required micro-services for launching GitLab instance.
  - Learn optional micro-services that doesn't need to run for debugging.
  - Learn how each micro-service is configured. (See `service/*/script/config` file)
  - Learn how to run an individual micro-service (e.g. `bin/web start_foreground`, `yarn dev-server`).
- **Develop fast**:
  - Parallelization is the first-class concept. You can maximize the advantage of multi-core machines.
  - You only restart the micro-service you're working on. You don't restart everything when GitLab instance does not work properly.
  - Try to cache data if available.
- **Easy to extend**:
  - You can setup and integrate third-party services easily such as minio.

## Prerequisite

1. Install [Devkitkat](https://github.com/dosuken123/devkitkat)
1. Install [prerequisites](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/prepare.md)
1. Prepare a locally-trusted development certificate with [mkcert](https://github.com/FiloSottile/mkcert)

```
shinya@shinya-XPS-15-9530:~/thin-gdk$ mkcert -install
Created a new local CA 💥
The local CA is now installed in the system trust store! ⚡️
The local CA is now installed in the Firefox and/or Chrome/Chromium trust store (requires browser restart)! 🦊

shinya@shinya-XPS-15-9530:~/thin-gdk$ mkcert local.gitlab.test *.local.gitlab.test

Created a new certificate valid for the following names 📜
 - "local.gitlab.test"
 - "*.local.gitlab.test"

Reminder: X.509 wildcards only go one level deep, so this won't match a.b.local.gitlab.test ℹ️

The certificate is at "./local.gitlab.test+1.pem" and the key at "./local.gitlab.test+1-key.pem" ✅

It will expire on 31 January 2026 🗓
```

## How to install

### 1 Clone sources

**Clone this kit**

```
git clone git@gitlab.com:shinya.maeda/thin-gdk.git
cd thin-gdk
```

**Clone sources**

```
devkitkat clone default
```

### 2. Configure services

```
devkitkat configure default
```

**Add `dummy0` interface at 172.16.123.1 permanently**

```
./services/system/script/add-test-domain
```

### 3. Create seed data

**Run background services for seeding development fixtures**

```
devkitkat start except-rails
```

**Seeding development data**

```
devkitkat seed rails
```

### 4. Run Gitlab instance

```
devkitkat start rails-debug
```

and access https://local.gitlab.test:8383

Alternatively, you can run all containers at once:

```
$ devkitkat start default
```

## Options

### Update source

```
$ devkitkat pull default
$ devkitkat reconfigure default
```

### Register and run GitLab-runner

```
# Compile a runner binary
$ devkitkat configure runner

# Register a new runner
$ devkitkat register runner

# Run a compiled runner binary
$ devkitkat start runner
```

### Run default services except specific one

```
# Run default services except runner
$ devkitkat start default --exclude runner
```

## Expose your local gitlab instance to world for integration test

Often you develop a feature with an external service such as GKE.
In this case, you have to expose your instance to the world, otherwirse clusters
on GKE cannot resolve the domain/ip on your local machine.
For such purpose, you can use third party tunnel service like [ngrok](https://ngrok.com/).

To expose your instance, execute:

```shell
ngrok http 8181 -subdomain=dosuken -region=ap
```

and add an environment variable to `.devkitkat.yml` for adding extra hosts in your rails instance:

```yaml
rails:
  RAILS_HOSTS: dosuken.ap.ngrok.io
```

## Command syntax

You can add and run a specific script for each service.

The command syntax can be represented as below.

```
$ devkitkat <script name> <service name>
```

For example, if you want to *start* *rails* app.

```
$ devkitkat start rails
```

If you want to login(*psql*) to *postgresql* console

```
$ devkitkat psql postgres
```

## Compatibility

We've confirmed that this kit is working on the following machines:

- Ubuntu 18.10

## How to develop faster (Use spring)

Spring allows you to develop much faster, especially if you frequently use `rake`, `rails` and `rspec` commands.
It's basically launched as a standard rails server, but the main difference is that it provides forked processes, instead ownself runs as a web server.
This means we can skip initialization steps of launching web server, which is required per `bundle exec rake/rails/rspec` currently.

To use spring server, please execute

```
$ devkitkat start spring
```

, and follow the instructions in the output. 

After the server has been started, you can execute commands with a forked process from the spring server, e.g.

```
> bin/rake db:migrate:status                    # Check migration status
> bin/rails console                             # Laucnh rails console
> bin/rspec spec/workers/archive_trace_worker_spec.rb  # Run a spec
```

If you don't see your change doesn't appear in results, try to re-launch spring server.
Since GitLab rails was quite complicated, reloading on the cache server doesn't work as expected, occasionally.

For the record, here is a benchmark

Before
```
> time bundle exec rspec /home/shinya/workspace/thin-gdk/gitlab-rails/spec/workers/archive_trace_worker_spec.rb

real    0m18.327s
user    0m11.081s
sys     0m2.142s
```

After
```
> time bin/rspec `/home/shinya/workspace/thin-gdk/gitlab-rails/spec/workers/archive_trace_worker_spec.rb`

real    0m7.070s
user    0m0.136s
sys     0m0.015s
```

### Reset the kit is easy and fast

Since all configuration scripts are parallelized, it's way faster to reset the kit than GDK.

### Why GNU parallel?

You can see a good answer [here](https://stackoverflow.com/a/28361402) about "xargs" vs "parallel"

### Directory structure

- script ... script for services
- data ... permanent storage. This will be removed with `clean` command. This will not be removed with `unconfigure` command.
- cache ... Temporary storage. This will be removed with `clean` command. This might be removed with `unconfigure` command.  Actual configuration files will be stored in this dir.
- example ... Config exmaples for services
- log ... log emitted by services and tool
- src ... source files for services

## License

MIT
