FROM ubuntu:bionic

ARG GO_VERSION=1.12
ARG RUBY_VERSION=2.6.3
ARG IDENTIFIER_PATH_IN_CONTAINER=/thin-gdk-in-container
ARG KIT_ROOT_IN_CONTAINER=/thin-gdk

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y parallel wget curl libreadline-dev zlib1g-dev libssl-dev \
        build-essential redis-server git libpq-dev ed libicu-dev \
        cmake libmysqlclient-dev libsqlite3-dev libre2-dev lsb-core iproute2 net-tools

RUN apt-get install -y tzdata libpam-krb5 libkrb5-dev

# Install GoLang
RUN cd tmp/
RUN wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz
RUN tar -xvf go${GO_VERSION}.linux-amd64.tar.gz
RUN mv go /usr/local
RUN ln -s /usr/local/go/bin/* /usr/local/bin

# Install system ruby
RUN cd ~ && \
    wget https://cache.ruby-lang.org/pub/ruby/${RUBY_VERSION%.*}/ruby-${RUBY_VERSION}.tar.gz && \
    tar -xvzf ruby-${RUBY_VERSION}.tar.gz && \
    cd ruby-${RUBY_VERSION} && \
    ./configure && \
    make && \
    make install

# Install bundler
RUN gem install bundler -v 1.17.3

# Install PostgreSQL
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
RUN wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | apt-key add -
RUN apt-get update && apt-get install -y postgresql-10 postgresql-client-10 postgresql-contrib-10

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && \
    apt-get install -y --no-install-recommends yarn

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && \
    apt-get install -y --no-install-recommends nodejs

EXPOSE 8181

ENV ENABLE_SPRING=1

COPY . ${KIT_ROOT_IN_CONTAINER}
WORKDIR ${KIT_ROOT_IN_CONTAINER}

# Create identifier that this VM is guest
RUN echo "true" > ${IDENTIFIER_PATH_IN_CONTAINER}
